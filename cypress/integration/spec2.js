// @ts-check
describe('page', () => {
    beforeEach(() => {
      cy.visit('index.html')
    })
  
    it('has p', () => {
      cy.contains('p', 'This is a test page')
    })
  })
  